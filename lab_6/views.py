from django.shortcuts import render

# Create your views here.
def index(request):
    response = {}
    return render(request, 'lab_6/Lab_6.html', response)
