from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from .views import index
from django.test import Client
from django.urls import resolve
# Create your tests here.

class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code, 200)
