import requests
import os
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
API_MAHASISWA_LIST_URL = "https://api.cs.ui.ac.id/siakngcs/mahasiswa-list/"
API_MAHASISWA_URL = "https://api.cs.ui.ac.id/siakngcs/mahasiswa/"

class CSUIhelper:
    class __CSUIhelper:
        def __init__(self):
            self.username = env("SSO_USERNAME")
            self.password = env("SSO_PASSWORD")
            self.client_id = 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'
            self.access_token = self.get_access_token()

        def get_access_token(self):
            try:
                url = "https://akun.cs.ui.ac.id/oauth/token/"

                payload = "username=" + self.username + "&password=" + self.password + "&grant_type=password"
                headers = {
                    'authorization': "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW43RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxuVHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==",
                    'cache-control': "no-cache",
                    'content-type': "application/x-www-form-urlencoded"
                }

                response = requests.request("POST", url, data=payload, headers=headers)

                return response.json()["access_token"]
            except Exception:
                raise Exception("username atau password sso salah, input : [{}, {}] {}".format(self.username, self.password, os.environ.items()))

        def get_client_id(self):
            return self.client_id

        def get_auth_param_dict(self):
            dict = {}
            acces_token = self.get_access_token()
            client_id = self.get_client_id()
            dict['access_token'] = acces_token
            dict['client_id'] = client_id

            return dict

        def get_mahasiswa_list(self, page):
            response = requests.get(API_MAHASISWA_LIST_URL,
                                    params={"access_token": self.access_token, "client_id": self.client_id, 'page':page})
            mahasiswa_list = response.json()["results"]
            return mahasiswa_list

        def validate_npm(self, npm):
            status_code = requests.get(API_MAHASISWA_URL+npm+'/',
                                    params={"access_token": self.access_token, "client_id": self.client_id}).status_code
            return status_code == 200

        def get_mahasiswa_data(self, npm):

            if not self.validate_npm(npm):
                return {} 

            response = requests.get(API_MAHASISWA_URL+npm+'/',
                                    params={"access_token": self.access_token, "client_id": self.client_id})
            data_json = response.json()
            return {
                'nama':data_json['nama'],
                'npm':data_json['npm'],
                'kota_lahir':data_json['kota_lahir'],
                'tgl_lahir':data_json['tgl_lahir'],
                'alamat':data_json['alamat_mhs'],
                'zipcode':data_json['kd_pos_mhs'],
                'program':data_json['program'][0]['nm_org']
            }


    instance = None

    def __init__(self):
        if not CSUIhelper.instance:
            CSUIhelper.instance = CSUIhelper.__CSUIhelper()