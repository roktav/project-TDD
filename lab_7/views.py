from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.http import Http404

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json
import requests

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
    mahasiswa_list = get_mahasiswa_page(1)
    friend_list = Friend.objects.all()

    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    html = 'lab_7/daftar_teman.html'
    response['friend_list'] = Friend.objects.all().order_by('npm')
    return render(request, html, response)

def get_friend_list(request):
    if(request.method == 'GET'):
        friend_list = []
        for i in Friend.objects.all().order_by('npm'):
            friend_list.append(model_to_dict(i))

        return JsonResponse({'status_code':200, 'friends':friend_list})
    raise Http404

def get_mahasiswa_page(page):
    return csui_helper.instance.get_mahasiswa_list(page)

def get_mahasiswa(request):
    if request.method == 'GET':
        page = request.GET.get('page')
        return JsonResponse({'status':200, 'page':page, 'mahasiswa':get_mahasiswa_page(page)})

@csrf_exempt
def add_friend(request):
        
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']

        if not check_friend(npm) and csui_helper.instance.validate_npm(npm):
            friend = Friend (friend_name=name, npm=npm)
            friend.save()
        
            return JsonResponse({
                'status': 200,
                'friend_name':friend.friend_name,
            })
    
    return JsonResponse({'error':'message'})

def delete_friend(request):
    if(request.method == 'GET'):
        if(request.GET.get('id')):
            data = get_object_or_404(Friend, id=request.GET.get('id'))
        elif(request.GET.get('npm')):
            data = get_object_or_404(Friend, npm=request.GET.get('npm'))
        
        data.delete()
        return JsonResponse({'status_code':200})

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': check_friend(npm)
    }
    return JsonResponse(data)

def check_friend(npm):
    return Friend.objects.filter(npm=npm).count() > 0 

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    st = struct[0]["fields"]
    st['id'] = struct[0]['pk']
    data = json.dumps(st)
    return data

def friend_page(request, npm):
    html = 'lab_7/friend_page.html'
    get_object_or_404(Friend, npm=npm)
    data = csui_helper.instance.get_mahasiswa_data(npm)
    
    try:
        API_REQ = 'https://maps.googleapis.com/maps/api/geocode/json?address={},+ID& \
                    key=AIzaSyADkxOuul50UxK2DZP6W2f0DmTM92RyS9k'.format(data['zipcode'])
        res = requests.request('GET', API_REQ).json()['results'][0]
    
        return render(request, html, {
                'friend':data,
                'longitude':res['geometry']['location']['lng'],
                'latitude':res['geometry']['location']['lat'],
            })
    except:
        raise Http404